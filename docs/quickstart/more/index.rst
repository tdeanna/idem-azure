==========
Learn More
==========

There are more resources available to continue learning about Idem and Idem-azure.

You can learn more about Idem at the `Idem Getting Start Guide.
<https://docs.idemproject.io/getting-started>`_

You can find more details about each of the :doc:`azure exec modules
</ref/exec/index>` and :doc:`azure state modules </ref/states/index>`

Contributing
++++++++++++

If you'd like to contribute to the **idem-azure** Idem module you can find more
information :doc:`here </topics/contributing>`

The **idem-azure** git repository is found `here.
<https://gitlab.com/vmware/idem/idem-azure/>`_
