import pytest

profile = {
    "client_id": "8c693aa5-8431-4344-9b04-0000000000",
    "secret": "LO_7Q~mUeC66gz1JnFfWvbBj96X0000000000",
    "subscription_id": "98bf10bb-cf16-0000-9ef3-7ccc82e5d45e",
    "tenant": "4566e90f-143b-1111-0000-5b4aa1d1edd2",
}


@pytest.mark.asyncio
async def test_gather(hub):
    p1 = await hub.acct.metadata.azure.gather(profile=profile)
    assert p1 is not None
    assert p1["attribute_value"] == "98bf10bb-cf16-0000-9ef3-7ccc82e5d45e"
    assert p1["attribute_key"] == "subscription_id"
    assert p1["provider"] == "AZURE"
