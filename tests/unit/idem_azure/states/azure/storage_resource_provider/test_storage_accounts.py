import pytest

from tests.unit.idem_azure.states.azure import unit_test_utils
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST_AND_FLAG


RESOURCE_NAME = "my-resource"
RESOURCE_ID_TEMPLATE = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Storage/storageAccounts/{account_name}"
RESOURCE_TYPE = "storage_resource_provider.storage_accounts"
RESOURCE_ID_PROPERTIES = {
    "account_name": "my-storage-account",
    "subscription_id": "my-subscription-id",
    "resource_group_name": "my-resource-group",
}
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "sku_tier": "Standard",
    "sku_name": "Standard_LRS",
    "account_kind": "BlockBlobStorage",
    "is_hns_enabled": True,
    "nfsv3_enabled": True,
    "enable_https_traffic_only": False,
    "min_tls_version": "TLS1_2",
    "allow_blob_public_access": True,
    "allow_shared_key_access": True,
    "routing": {
        "publish_microsoft_endpoints": False,
        "publish_internet_endpoints": True,
        "routing_choice": "InternetRouting",
    },
    "encryption_service": {
        "encryption_key_source": "Microsoft.Keyvault",
        "queue_encryption_key_type": "Account",
        "table_encryption_key_type": "Service",
        "blob_encryption_key_type": "Account",
        "file_encryption_key_type": "Account",
    },
    "customer_managed_key": {
        "key_vault_uri": "myvault8569.vault.azure.net",
        "key_name": "wrappingKey",
        "key_version": "",
        "federated_identity_client_id": "f83c6b1b-4d34-47e4-bb34-9d83df58b540",
        "user_assigned_identity_id": "/subscriptions/{subscription-id}/resourceGroups/res9101/providers/Microsoft.ManagedIdentity/userAssignedIdentities/{managed-identity-name}",
    },
    "key_policy": {"key_expiration_period_in_days": 5},
    "azure_files_authentication": {
        "directory_service_options": "AADDS",
        "active_directory_properties": {
            "domain_guid": "asdadasd23423r2ff2gg2",
            "domain_name": "fdsfsd.com",
            "azure_storage_sid": "sdfsdfsgdfgh",
            "domain_sid": "fggdghrghrh",
            "forest_name": "adforest",
            "netbios_domain_name": "test.com",
        },
    },
}
RESOURCE_PARAMETERS_RAW = {
    "sku": {
        "name": "Standard_LRS",
        "tier": "Standard",
    },
    "kind": "BlockBlobStorage",
    "location": "eastus",
    "properties": {
        "isHnsEnabled": True,
        "isNfsV3Enabled": True,
        "supportsHttpsTrafficOnly": False,
        "minimumTlsVersion": "TLS1_2",
        "allowBlobPublicAccess": True,
        "allowSharedKeyAccess": True,
        "routingPreference": {
            "publishMicrosoftEndpoints": False,
            "publishInternetEndpoints": True,
            "routingChoice": "InternetRouting",
        },
        "encryption": {
            "services": {
                "queue": {"keyType": "Account"},
                "table": {"keyType": "Service"},
                "blob": {"keyType": "Account"},
                "file": {"keyType": "Account"},
            },
            "keyvaultproperties": {
                "keyvaulturi": "myvault8569.vault.azure.net",
                "keyname": "wrappingKey",
                "keyversion": "",
            },
            "keySource": "Microsoft.Keyvault",
            "identity": {
                "userAssignedIdentity": "/subscriptions/{subscription-id}/resourceGroups/res9101/providers/Microsoft.ManagedIdentity/userAssignedIdentities/{managed-identity-name}",
                "federatedIdentityClientId": "f83c6b1b-4d34-47e4-bb34-9d83df58b540",
            },
        },
        "keyPolicy": {"keyExpirationPeriodInDays": 5},
        "azureFilesIdentityBasedAuthentication": {
            "directoryServiceOptions": "AADDS",
            "activeDirectoryProperties": {
                "domainGuid": "asdadasd23423r2ff2gg2",
                "domainName": "fdsfsd.com",
                "azureStorageSid": "sdfsdfsgdfgh",
                "domainSid": "fggdghrghrh",
                "forestName": "adforest",
                "netBiosDomainName": "test.com",
            },
        },
    },
}
RESOURCE_PARAMETERS_UPDATE = {
    "location": "eastus",
    "sku_tier": "Standard",
    "sku_name": "Standard_LRS",
    "account_kind": "BlockBlobStorage",
    "is_hns_enabled": True,
    "nfsv3_enabled": True,
    "enable_https_traffic_only": True,
    "min_tls_version": "TLS1_1",
    "allow_blob_public_access": False,
    "allow_shared_key_access": False,
    "routing": {
        "publish_microsoft_endpoints": True,
        "publish_internet_endpoints": False,
        "routing_choice": "InternetRouting",
    },
    "encryption_service": {
        "encryption_key_source": "Microsoft.Keyvault",
        "queue_encryption_key_type": "Account",
        "table_encryption_key_type": "Service",
        "blob_encryption_key_type": "Account",
        "file_encryption_key_type": "Account",
    },
    "customer_managed_key": {
        "key_vault_uri": "myvault8569.vault.azure.net",
        "key_name": "wrappingKey",
        "key_version": "",
        "federated_identity_client_id": "f83c6b1b-4d34-47e4-bb34-9d83df58b540",
        "user_assigned_identity_id": "/subscriptions/{subscription-id}/resourceGroups/res9101/providers/Microsoft.ManagedIdentity/userAssignedIdentities/{managed-identity-name}",
    },
    "key_policy": {"key_expiration_period_in_days": 5},
    "azure_files_authentication": {
        "directory_service_options": "AADDS",
        "active_directory_properties": {
            "domain_guid": "asdadasd23423r2ff2gg2",
            "domain_name": "fdsfsd.com",
            "azure_storage_sid": "sdfsdfsgdfgh",
            "domain_sid": "fggdghrghrh",
            "forest_name": "adforest",
            "netbios_domain_name": "test.com",
        },
    },
}
RESOURCE_PARAMETERS_UPDATE_RAW = {
    "sku": {
        "name": "Standard_LRS",
        "tier": "Standard",
    },
    "kind": "BlockBlobStorage",
    "location": "eastus",
    "properties": {
        "isHnsEnabled": True,
        "isNfsV3Enabled": True,
        "supportsHttpsTrafficOnly": True,
        "minimumTlsVersion": "TLS1_1",
        "allowBlobPublicAccess": False,
        "allowSharedKeyAccess": False,
        "routingPreference": {
            "publishMicrosoftEndpoints": True,
            "publishInternetEndpoints": False,
            "routingChoice": "InternetRouting",
        },
        "encryption": {
            "services": {
                "queue": {"keyType": "Account"},
                "table": {"keyType": "Service"},
                "blob": {"keyType": "Account"},
                "file": {"keyType": "Account"},
            },
            "keyvaultproperties": {
                "keyvaulturi": "myvault8569.vault.azure.net",
                "keyname": "wrappingKey",
                "keyversion": "",
            },
            "keySource": "Microsoft.Keyvault",
            "identity": {
                "userAssignedIdentity": "/subscriptions/{subscription-id}/resourceGroups/res9101/providers/Microsoft.ManagedIdentity/userAssignedIdentities/{managed-identity-name}",
                "federatedIdentityClientId": "f83c6b1b-4d34-47e4-bb34-9d83df58b540",
            },
        },
        "keyPolicy": {"keyExpirationPeriodInDays": 5},
        "azureFilesIdentityBasedAuthentication": {
            "directoryServiceOptions": "AADDS",
            "activeDirectoryProperties": {
                "domainGuid": "asdadasd23423r2ff2gg2",
                "domainName": "fdsfsd.com",
                "azureStorageSid": "sdfsdfsgdfgh",
                "domainSid": "fggdghrghrh",
                "forestName": "adforest",
                "netBiosDomainName": "test.com",
            },
        },
    },
}


@pytest.fixture(scope="module", autouse=True)
def setup_subscription_id(ctx):
    RESOURCE_ID_PROPERTIES["subscription_id"] = ctx["acct"]["subscription_id"]


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, ctx):
    """
    Test 'present' state of storage accounts. When a resource does not exist, 'present' should create the resource.
    """

    await unit_test_utils.test_present_resource_not_exists_create_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, ctx):
    """
    Test 'present' state of storage accounts. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """

    await unit_test_utils.test_present_resource_exists_update_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_UPDATE_RAW,
        RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, ctx):
    """
    Test 'absent' state of storage accounts. When a resource does not exist, 'absent' should just return success.
    """

    await unit_test_utils.test_absent_resource_not_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, ctx):
    """
    Test 'absent' state of storage accounts. When a resource exists, 'absent' should delete the resource.
    """

    await unit_test_utils.test_absent_resource_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    """
    Test 'describe' state of storage accounts.
    """

    list_url_format = "{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Storage/storageAccounts?api-version={api_version}"
    await unit_test_utils.test_describe_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
        list_url_format=list_url_format,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
# same behaviour with or without resource id flag
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_resource_id_not_passed_successful_create(
    hub, ctx, fake_acct_data, __test, __resource_id_flag
):
    unit_test_utils.test_resource_id_not_passed_successful_create(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
def test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
    hub, ctx, fake_acct_data, __test
):
    unit_test_utils.test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
        hub,
        ctx,
        fake_acct_data,
        __test,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_without_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )
