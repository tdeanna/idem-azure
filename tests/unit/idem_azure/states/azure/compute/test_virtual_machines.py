import copy

import pytest

from tests.unit.idem_azure.states.azure import unit_test_utils
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST_AND_FLAG

RESOURCE_TYPE = "compute.virtual_machines"
API_VERSION = "2022-03-01"
RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
RESOURCE_ID_TEMPLATE = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Compute/virtualMachines/{virtual_machine_name}"
VM_NAME = "my-vm"
RESOURCE_ID_PROPERTIES = {
    "subscription_id": "my-sub-id",
    "virtual_machine_name": VM_NAME,
    "resource_group_name": RESOURCE_GROUP_NAME,
}
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "virtual_machine_size": "Standard_B1ls",
    "network_interface_ids": [
        "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Network/networkInterfaces/nic-id-1"
    ],
    "storage_image_reference": {
        "image_sku": "18.04-LTS",
        "image_publisher": "Canonical",
        "image_version": "latest",
        "image_offer": "UbuntuServer",
    },
    "storage_os_disk": {
        "storage_account_type": "Standard_LRS",
        "disk_name": "myVMosdisk",
        "disk_caching": "ReadWrite",
        "disk_size_in_GB": 30,
        "disk_create_option": "FromImage",
        "disk_delete_option": "Detach",
    },
    "storage_data_disks": [
        {
            "disk_name": "data-disk-1",
            "disk_size_in_GB": 2,
            "disk_logical_unit_number": 1,
            "disk_caching": "None",
            "disk_create_option": "Attach",
            "disk_delete_option": "Detach",
            "storage_account_type": "Premium_LRS",
            "disk_id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Compute/disks/data-disk-1",
        },
        {
            "disk_name": "data-disk-2",
            "disk_size_in_GB": 4,
            "disk_logical_unit_number": 2,
            "disk_caching": "ReadWrite",
            "disk_create_option": "Empty",
            "storage_account_type": "Premium_LRS",
            "disk_delete_option": "Detach",
            "disk_id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Compute/disks/data-disk-2",
        },
    ],
    "os_profile": {
        "admin_username": "admin123",
        "computer_name": "myVM",
        "admin_password": "adminPassword",
    },
    "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
}

RESOURCE_PARAMETERS_RAW = {
    "location": "eastus",
    "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
    "properties": {
        "hardwareProfile": {"vmSize": "Standard_B1ls"},
        "storageProfile": {
            "imageReference": {
                "sku": "18.04-LTS",
                "publisher": "Canonical",
                "version": "latest",
                "offer": "UbuntuServer",
            },
            "osDisk": {
                "caching": "ReadWrite",
                "managedDisk": {"id": None, "storageAccountType": "Standard_LRS"},
                "name": "myVMosdisk",
                "createOption": "FromImage",
                "deleteOption": "Detach",
                "diskSizeGB": 30,
            },
            "dataDisks": [
                {
                    "lun": 1,
                    "name": "data-disk-1",
                    "createOption": "Attach",
                    "caching": "None",
                    "managedDisk": {
                        "storageAccountType": "Premium_LRS",
                        "id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Compute/disks/data-disk-1",
                    },
                    "diskSizeGB": 2,
                    "deleteOption": "Detach",
                },
                {
                    "lun": 2,
                    "name": "data-disk-2",
                    "createOption": "Empty",
                    "caching": "ReadWrite",
                    "managedDisk": {
                        "storageAccountType": "Premium_LRS",
                        "id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Compute/disks/data-disk-2",
                    },
                    "diskSizeGB": 4,
                    "deleteOption": "Detach",
                },
            ],
        },
        "osProfile": {
            "adminUsername": "admin123",
            "computerName": "myVM",
            "adminPassword": "adminPassword",
        },
        "networkProfile": {
            "networkInterfaces": [
                {
                    "id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Network/networkInterfaces/nic-id-1",
                    "properties": {"primary": True},
                }
            ]
        },
    },
}

RESOURCE_PARAMETERS_UPDATE = {
    "location": "eastus",
    "network_interface_ids": [
        "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Network/networkInterfaces/nic-id-2"
    ],
    "virtual_machine_size": "Standard_B1s",
    "os_profile": {
        "admin_username": "admin123",
        "computer_name": "myVM",
    },
    "storage_image_reference": {
        "image_sku": "18.04-LTS",
        "image_publisher": "Canonical",
        "image_version": "latest",
        "image_offer": "UbuntuServer",
    },
    "storage_os_disk": {
        "storage_account_type": "Standard_LRS",
        "disk_name": "myVMosdisk",
        "disk_caching": "ReadWrite",
        "disk_size_in_GB": 30,
        "disk_create_option": "FromImage",
        "disk_delete_option": "Detach",
    },
    "storage_data_disks": [
        {
            "disk_name": "data-disk-3",
            "disk_size_in_GB": 8,
            "disk_logical_unit_number": 4,
            "disk_caching": "ReadWrite",
            "disk_create_option": "Empty",
            "storage_account_type": "Premium_LRS",
            "disk_delete_option": "Detach",
            "disk_id": "test-id",
        }
    ],
    "tags": {"tag-new-key": "tag-new-value"},
}

RESOURCE_PARAMETERS_UPDATE_RAW = {
    "location": "eastus",
    "tags": {"tag-new-key": "tag-new-value"},
    "properties": {
        "hardwareProfile": {"vmSize": "Standard_B1s"},
        "storageProfile": {
            "imageReference": {
                "sku": "18.04-LTS",
                "publisher": "Canonical",
                "version": "latest",
                "offer": "UbuntuServer",
            },
            "osDisk": {
                "caching": "ReadWrite",
                "managedDisk": {"id": None, "storageAccountType": "Standard_LRS"},
                "name": "myVMosdisk",
                "createOption": "FromImage",
                "deleteOption": "Detach",
                "diskSizeGB": 30,
            },
            "dataDisks": [
                {
                    "lun": 4,
                    "name": "data-disk-3",
                    "createOption": "Empty",
                    "caching": "ReadWrite",
                    "managedDisk": {
                        "id": "test-id",
                        "storageAccountType": "Premium_LRS",
                    },
                    "diskSizeGB": 8,
                    "deleteOption": "Detach",
                }
            ],
        },
        "osProfile": {"adminUsername": "admin123", "computerName": "myVM"},
        "networkProfile": {
            "networkInterfaces": [
                {
                    "id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Network/networkInterfaces/nic-id-2",
                    "properties": {"primary": True},
                }
            ]
        },
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, ctx):
    """
    Test 'present' state of virtual machine. When a resource does not exist, 'present' should create the resource.
    """
    await unit_test_utils.test_present_resource_not_exists_create_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, ctx):
    """
    Test 'present' state of virtual machine. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    await unit_test_utils.test_present_resource_exists_update_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_UPDATE_RAW,
        RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, ctx):
    """
    Test 'absent' state of virtual machines. When a resource does not exist, 'absent' should just return success.
    """

    await unit_test_utils.test_absent_resource_not_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, ctx):
    """
    Test 'absent' state of virtual machines. When a resource exists, 'absent' should delete the resource.
    """

    await unit_test_utils.test_absent_resource_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    """
    Test 'describe' state of virtual machines.
    """
    list_url_format = "{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Compute/virtualMachines?api-version={api_version}"
    resource_id_properties = copy.deepcopy(RESOURCE_ID_PROPERTIES)
    resource_id_properties["subscription_id"] = ctx.acct.subscription_id
    await unit_test_utils.test_describe_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        resource_id_properties,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS,
        list_url_format=list_url_format,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
# same behaviour with or without resource id flag
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_resource_id_not_passed_successful_create(
    hub, ctx, fake_acct_data, __test, __resource_id_flag
):
    unit_test_utils.test_resource_id_not_passed_successful_create(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
def test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
    hub, ctx, fake_acct_data, __test
):
    unit_test_utils.test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
        hub,
        ctx,
        fake_acct_data,
        __test,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_without_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_without_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_RAW,
    )
