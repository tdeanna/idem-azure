import pytest

from tests.unit.idem_azure.states.azure import unit_test_utils

SHORT_RESOURCE_TYPE = "network.network_interfaces"
AZURE_RESOURCE_TYPE = f"azure.{SHORT_RESOURCE_TYPE}"
RESOURCE_NAME = "unit-test-nic-resource-name"
RESOURCE_ID_FORMAT = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/networkInterfaces/{network_interface_name}"

RESOURCE_ID_PROPERTIES = {
    "subscription_id": "unit-test-nic-subscription-id",
    "resource_group_name": "unit-test-nic-resource-group",
    "network_interface_name": "unit-test-nic-name",
}

RESOURCE_PARAMETERS_PRESENT = {
    "location": "eastus",
    "tags": {"tag-key": "tag-value"},
    "ip_configurations": [
        {
            "name": "unit-test-nic-ipc",
            "private_ip_address": "10.0.1.1",
            "private_ip_address_allocation": "Dynamic",
            "subnet_id": "subnet_id",
            "primary": "true",
            "private_ip_address_version": "IPv4",
            "public_ip_address_id": "public_ip_address_resource_id",
        }
    ],
    "enable_accelerated_networking": True,
    "dns_settings": {
        "dns_servers": ["168.63.129.16"],
        "internal_dns_name_label": "unit-test-nic-internal-dns-label-1",
    },
    "network_security_group_id": "network_security_group_resource_id",
}


RESOURCE_PARAMETERS_RAW = {
    "location": "eastus",
    "tags": {"tag-key": "tag-value"},
    "properties": {
        "ipConfigurations": [
            {
                "name": "unit-test-nic-ipc",
                "properties": {
                    "privateIPAddress": "10.0.1.1",
                    "privateIPAllocationMethod": "Dynamic",
                    "subnet": {
                        "id": "subnet_id",
                    },
                    "primary": "true",
                    "privateIPAddressVersion": "IPv4",
                    "publicIPAddress": {"id": "public_ip_address_resource_id"},
                },
            }
        ],
        "networkSecurityGroup": {
            "id": "network_security_group_resource_id",
        },
        "dnsSettings": {
            "dnsServers": ["168.63.129.16"],
            "internalDnsNameLabel": "unit-test-nic-internal-dns-label-1",
        },
        "enableAcceleratedNetworking": True,
        "networkSecurityGroup": {
            "id": "network_security_group_resource_id",
        },
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists_create(hub, ctx):
    """
    Test 'present' state of network interfaces. When a resource does not exist, 'present' should create the resource.
    """
    await unit_test_utils.test_present_resource_not_exists_create_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        RESOURCE_NAME,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_PRESENT,
    )


@pytest.mark.asyncio
async def test_present_resource_exists_update(hub, ctx):
    """
    Test 'present' state of network interface. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    resource_parameters_update_present = {
        "ip_configurations": [
            {
                "name": "my-nic-ipc-updated",
                "private_ip_address": "10.0.1.8",
                "private_ip_address_allocation": "Static",
                "subnet_id": "subnet_id_updated",
                "primary": "true",
                "private_ip_address_version": "IPv4",
                "public_ip_address_id": "public_ip_address_resource_id",
            }
        ],
        "location": "eastus",
        "tags": {"tag-new-key": "tag-new-value"},
        "enable_accelerated_networking": False,
        "dns_settings": {
            "dns_servers": ["168.63.129.16"],
            "internal_dns_name_label": "unit-test-nic-internal-dns-label-1",
        },
        "network_security_group_id": "network_security_group_resource_id",
    }

    resource_parameters_update_raw = {
        "properties": {
            "ipConfigurations": [
                {
                    "name": "my-nic-ipc-updated",
                    "properties": {
                        "privateIPAddress": "10.0.1.8",
                        "privateIPAllocationMethod": "Static",
                        "subnet": {
                            "id": "subnet_id_updated",
                        },
                        "primary": "true",
                        "privateIPAddressVersion": "IPv4",
                        "publicIPAddress": {"id": "public_ip_address_resource_id"},
                    },
                }
            ],
            "networkSecurityGroup": {
                "id": "network_security_group_resource_id",
            },
            "dnsSettings": {
                "dnsServers": ["168.63.129.16"],
                "internalDnsNameLabel": "unit-test-nic-internal-dns-label-1",
            },
            "enableAcceleratedNetworking": False,
        },
        "location": "eastus",
        "tags": {"tag-new-key": "tag-new-value"},
    }

    await unit_test_utils.test_present_resource_exists_update_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        RESOURCE_NAME,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_PRESENT,
        resource_parameters_update_raw,
        resource_parameters_update_present,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, ctx):
    """
    Test 'absent' state of network interfaces. When a resource does not exist, 'absent' should just return success.
    """
    await unit_test_utils.test_absent_resource_not_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        RESOURCE_NAME,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, ctx):
    """
    Test 'absent' state of network interfaces. When a resource exists, 'absent' should delete the resource.
    """
    await unit_test_utils.test_absent_resource_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        RESOURCE_NAME,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_PRESENT,
    )


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    """
    Test 'describe' state of network interfaces.
    """
    list_url_format = "{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Network/networkInterfaces?api-version={api_version}"
    await unit_test_utils.test_describe_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_FORMAT,
        SHORT_RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS_RAW,
        RESOURCE_PARAMETERS_PRESENT,
        list_url_format=list_url_format,
    )
