from typing import Any
from typing import Dict


class Cleaner:
    def __init__(self, hub, ctx, idem_cli):
        self._resources_to_delete = {}
        self.hub = hub
        self.ctx = ctx
        self.idem_cli = idem_cli

    def mark_for_deletion(self, res: Dict[str, Any], res_type: str):
        self._resources_to_delete[res.get("resource_id")] = {
            "name": res.get("name"),
            "type": res_type,
        }

    def mark_deleted(self, resource_id: str):
        self._resources_to_delete.pop(resource_id, None)

    async def clean_resources(self):
        for resource_id, resource_info in self._resources_to_delete.items():
            ret = await self.hub.tool.azure.test_utils.call_absent(
                ctx=self.ctx,
                idem_cli=self.idem_cli,
                resource_type=resource_info.get("type"),
                name=resource_info.get("name"),
                resource_id=resource_id,
                wait_for_absent=True,
            )
            assert ret["result"], ret["comment"]
            assert not ret.get("new_state")
